# Container assignment

With the acquired knowledge, define a docker configuration file that allows running multi-containers to upload a web application that requires / relying on a data persistence model.

## How should you do this assignment?

* GitLab should be used, with an account created using your Avenue Code e-mail;
* DockerHub should be used, with an account created using your Avenue Code e-mail;
* It doesn't needed to create an app from scratch;
* This assignment will assess your knowledge of creating and running containers-based apps and how different containers communicate to perform a task.

## What do we expect to see in this assignment?

* The docker configuration file must be able to spin up a full-fledged web application with its persistence layer dependency. 
* Any web application can be used at your choice;
* All used images must be created as a layer on top of the official ones and added to your namespace on the dockerhub;
* Your docker-compose must point to your images in the docker registry;
* Your docker-compose should run the same as it does in your local docker installation on the play with docker platform;

## What should be submitted?

* A link to a GitLab repo where we can browse and evaluate the code;
* A link to the docker images pushed to the Docker Hub;
* The step by step of the commands you used for this activity and what is needed to play your application in play with docker platform.


## Docker images repository


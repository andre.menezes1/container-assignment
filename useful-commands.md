# Docker

### Build docker images
```
docker built -t andremenezesac/[image-name]:[image-tag] . 
```
### Push images to Dockerhub
```
docker push andremenezesac/[image-name]:[image-tag]
```

### Running container with Docker compose
```
docker compose up # use -d to running in background
```
### Check if containers are running
```
docker container ls 
```

### Seed sample data to database
```
curl --location --request POST 'http://ip172-18-0-47-c3oor87qf8u000e8lo30-8080.direct.labs.play-with-docker.com/api/tutorials' \
     --header 'Content-Type: application/json' \
     --data-raw '{
        "title": "How to create Docker images",
        "description": "A complete article to guide you and your team to success"
    }'
```

### Making a GET request from our API
```
http://[play-with-docker-url]/api/tutorials
```